// @flow
//default imports
import 'react-native-gesture-handler';

import React from 'react';

//components and files
import GlobalState from './src/context/GlobalState';
import Order from './src/screens/Order';
import Cart from './src/screens/Cart';

//3rd party
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

const App: () => React$Node = () => {
  return (
    <GlobalState>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Order">
          <Stack.Screen
            name="Order"
            component={Order}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="Cart"
            component={Cart}
            options={{
              headerShown: false,
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </GlobalState>
  );
};

export default App;
