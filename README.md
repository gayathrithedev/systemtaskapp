# System Task App

Order Screen lets the user to order items and user can modify the items from cart screen.

# Files

![Order Screen](https://drive.google.com/file/d/1jA2lk-Mdna1hK93imYIpRWAB9SH_IkKe/view?usp=sharing)

![Cart Screen](https://drive.google.com/file/d/1fmNzuLYCAxCsEDGL2UlgCtc9HUmeXkhV/view?usp=sharing)