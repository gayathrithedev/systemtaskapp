// @flow
import React, {useState} from 'react';
import OrderFoodContext from './orderFoodContext';

const GlobalState = (props) => {
  const items = [
    {
      id: '001',
      name: 'Green salad',
      ingredients: [
        'capsicum, spinach, beans, green peas, cucumber, coconut dressing, and nuts dressing',
      ],
      contains: ['V'],
      category: 'Salad',
      cost: 7,
    },
    {
      id: '002',
      name: 'Malai kofta',
      ingredients: ['pure kofta, butter, beans, spicy masala'],
      contains: ['V', 'D'],
      category: 'Salad',
      cost: 8,
    },
    {
      id: '003',
      name: 'Chicken salad',
      ingredients: [
        'chicken, curd dressing, bell peppers, green peas, cucumber',
      ],
      contains: ['N', 'D'],
      category: 'Salad',
      cost: 9,
    },
    {
      id: '004',
      name: 'Green salad',
      ingredients: [
        'capsicum, spinach, beans, green peas, cucumber, coconut dressing, and nuts dressing',
      ],
      contains: ['V'],
      category: 'Salad',
      cost: 7,
    },
    {
      id: '005',
      name: 'Malai kofta',
      ingredients: ['pure kofta, butter, beans, spicy masala'],
      contains: ['V', 'D'],
      category: 'Salad',
      cost: 8,
    },
    {
      id: '006',
      name: 'Chicken salad',
      ingredients: [
        'chicken, curd dressing, bell peppers, green peas, cucumber',
      ],
      contains: ['N', 'D'],
      category: 'Salad',
      cost: 9,
    },
    {
      id: '007',
      name: 'Green salad',
      ingredients: [
        'capsicum, spinach, beans, green peas, cucumber, coconut dressing, and nuts dressing',
      ],
      contains: ['V'],
      category: 'Salad',
      cost: 7,
    },
    {
      id: '008',
      name: 'Malai kofta',
      ingredients: ['pure kofta, butter, beans, spicy masala'],
      contains: ['V', 'D'],
      category: 'Salad',
      cost: 8,
    },
    {
      id: '009',
      name: 'Chicken salad',
      ingredients: [
        'chicken, curd dressing, bell peppers, green peas, cucumber',
      ],
      contains: ['N', 'D'],
      category: 'Salad',
      cost: 9,
    },
    {
      id: '010',
      name: 'Green salad',
      ingredients: [
        'capsicum, spinach, beans, green peas, cucumber, coconut dressing, and nuts dressing',
      ],
      contains: ['V'],
      category: 'Salad',
      cost: 7,
    },
    {
      id: '011',
      name: 'Malai kofta',
      ingredients: ['pure kofta, butter, beans, spicy masala'],
      contains: ['V', 'D'],
      category: 'Salad',
      cost: 8,
    },
    {
      id: '012',
      name: 'Chicken salad',
      ingredients: [
        'chicken, curd dressing, bell peppers, green peas, cucumber',
      ],
      contains: ['N', 'D'],
      category: 'Salad',
      cost: 9,
    },
  ];

  const [cartState, setUpdatecart] = useState({
    cart: [],
    total: 0,
    totalCost: 0,
  });

  const addItemToCart = (item) => {
    const cart = [...cartState.cart];
    const checkItemExists =
      cart && cart.findIndex((data) => data.id === item.id);
    if (checkItemExists < 0) {
      const updateItem = item;
      updateItem.quantity = 1;
      const total = cartState.total + 1;
      const newCost = cartState.totalCost + item.cost;
      setUpdatecart({
        cart: [...cartState.cart, updateItem],
        total: total,
        totalCost: newCost,
      });
    } else {
      const updateItem = {
        ...cart[checkItemExists],
      };
      updateItem.quantity += 1;
      cart[checkItemExists] = updateItem;
      const total = cartState.total + 1;
      const newCost = cartState.totalCost + item.cost;
      setUpdatecart({
        cart: [...cart],
        total: total,
        totalCost: newCost,
      });
    }
  };

  const removeItemFromCart = (itemId) => {
    const cart = [...cartState.cart];
    const deleteItem = cart.find((data) => data.id === itemId);
    if (deleteItem.quantity === 1) {
      const newCart = cart.filter((data) => data.id !== itemId);
      const totalCount = cartState.total - 1;
      const newCost = cartState.totalCost - deleteItem.cost;
      setUpdatecart({
        cart: newCart,
        total: totalCount,
        totalCost: newCost,
      });
    } else {
      const updateItem = cart.find((data) => data.id === itemId);
      const index = cart.findIndex((data) => data.id === itemId);
      updateItem.quantity -= 1;
      cart[index] = updateItem;
      const totalCount = cartState.total - 1;
      const newCost = cartState.totalCost - updateItem.cost;
      setUpdatecart({
        cart: cart,
        total: totalCount,
        totalCost: newCost,
      });
    }
  };

  return (
    <OrderFoodContext.Provider
      value={{
        items: items,
        cart: cartState.cart,
        addItemToCart: addItemToCart,
        removeItemFromCart: removeItemFromCart,
        total: cartState.total,
        totalCost: cartState.totalCost,
      }}>
      {props.children}
    </OrderFoodContext.Provider>
  );
};

export default GlobalState;
