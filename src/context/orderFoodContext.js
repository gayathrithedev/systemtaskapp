// @flow
import React from 'react';

export default React.createContext({
  items: [
    {
      id: '001',
      name: 'Green salad',
      ingredients: [
        'capsicum, spinach, beans, green peas, cucumber, coconut dressing, and nuts dressing',
      ],
      contains: ['V'],
      category: 'Salad',
      cost: 7,
    },
    {
      id: '002',
      name: 'Malai kofta',
      ingredients: ['pure kofta, butter, beans, spicy masala'],
      contains: ['V', 'D'],
      category: 'Salad',
      cost: 8,
    },
    {
      id: '003',
      name: 'Chicken salad',
      ingredients: [
        'chicken, curd dressing, bell peppers, green peas, cucumber',
      ],
      contains: ['N', 'D'],
      category: 'Salad',
      cost: 9,
    },
    {
      id: '004',
      name: 'Green salad',
      ingredients: [
        'capsicum, spinach, beans, green peas, cucumber, coconut dressing, and nuts dressing',
      ],
      contains: ['V'],
      category: 'Salad',
      cost: 7,
    },
    {
      id: '005',
      name: 'Malai kofta',
      ingredients: ['pure kofta, butter, beans, spicy masala'],
      contains: ['V', 'D'],
      category: 'Salad',
      cost: 8,
    },
    {
      id: '006',
      name: 'Chicken salad',
      ingredients: [
        'chicken, curd dressing, bell peppers, green peas, cucumber',
      ],
      contains: ['N', 'D'],
      category: 'Salad',
      cost: 9,
    },
    {
      id: '007',
      name: 'Green salad',
      ingredients: [
        'capsicum, spinach, beans, green peas, cucumber, coconut dressing, and nuts dressing',
      ],
      contains: ['V'],
      category: 'Salad',
      cost: 7,
    },
    {
      id: '008',
      name: 'Malai kofta',
      ingredients: ['pure kofta, butter, beans, spicy masala'],
      contains: ['V', 'D'],
      category: 'Salad',
      cost: 8,
    },
    {
      id: '009',
      name: 'Chicken salad',
      ingredients: [
        'chicken, curd dressing, bell peppers, green peas, cucumber',
      ],
      contains: ['N', 'D'],
      category: 'Salad',
      cost: 9,
    },
    {
      id: '010',
      name: 'Green salad',
      ingredients: [
        'capsicum, spinach, beans, green peas, cucumber, coconut dressing, and nuts dressing',
      ],
      contains: ['V'],
      category: 'Salad',
      cost: 7,
    },
    {
      id: '011',
      name: 'Malai kofta',
      ingredients: ['pure kofta, butter, beans, spicy masala'],
      contains: ['V', 'D'],
      category: 'Salad',
      cost: 8,
    },
    {
      id: '012',
      name: 'Chicken salad',
      ingredients: [
        'chicken, curd dressing, bell peppers, green peas, cucumber',
      ],
      contains: ['N', 'D'],
      category: 'Salad',
      cost: 9,
    },
  ],
  cart: [],
  total: 0,
  totalCost: 0,
  addItemToCart: (item) => {},
  removeItemFromCart: (itemId) => {},
});
