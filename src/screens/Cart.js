// @flow
import React, {useState} from 'react';
import {View, Text, TouchableOpacity, StyleSheet, Alert} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';

import OrderFoodContext from '../context/orderFoodContext';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import FoodItemList from '../components/FoodItemList';
import CustomStatusBar from '../components/CustomStatusBar';

const Cart = ({navigation}) => {
  const [loadMore, setLoadMore] = useState(false);
  const [deliveryOption, setDeliveryOption] = useState('dinein');

  const loadFoodListItem = (data) => {
    const firstTwoItems = data.slice(0, 2);
    if (loadMore) {
      return data.map((item) => (
        <FoodItemList item={item} key={item.id} isCart={true} />
      ));
    }
    return firstTwoItems.map((item) => (
      <FoodItemList item={item} key={item.id} isCart={true} />
    ));
  };

  return (
    <View style={styles.wrapper}>
      <CustomStatusBar backgroundColor="#151515" barStyle="light-content" />
      <OrderFoodContext.Consumer>
        {(conApi) => (
          <>
            <ScrollView style={styles.wrapper}>
              <View style={[styles.wrapper, styles.container]}>
                <View style={styles.headerBackground}>
                  <View style={styles.header}>
                    <TouchableOpacity
                      onPress={() => navigation.navigate('Order')}>
                      <Icon name="arrow-left" size={26} color="#fcfcfc" />
                    </TouchableOpacity>
                    <Text style={styles.heading}>My Cart</Text>
                  </View>
                  <View style={styles.totalCostWrapper}>
                    <View style={styles.totalCost}>
                      <Text style={styles.totalText}>Total Cost</Text>
                      <Text style={styles.priceText}>€{conApi.totalCost}</Text>
                    </View>
                  </View>
                </View>
                <View style={styles.cartItems}>
                  <Text style={styles.subheading}>Review Orders</Text>
                  {loadFoodListItem(conApi.cart)}
                </View>
                {conApi.cart.length > 2 ? (
                  <View style={styles.showMoreWrapper}>
                    <TouchableOpacity
                      style={styles.showMoreButton}
                      onPress={() => {
                        setLoadMore(!loadMore);
                      }}>
                      <Text style={styles.showMoreText}>
                        {loadMore ? 'Show less' : 'Show more'}
                      </Text>
                    </TouchableOpacity>
                  </View>
                ) : null}
                <Text style={[styles.subheading, styles.deliverySubheading]}>
                  Delivery Options
                </Text>
                <View style={styles.deliveryOptionsWrapper}>
                  <View style={styles.deliveryOption}>
                    <Icon name="table" size={24} />
                    <Text style={styles.deliveryText}>Dine-In</Text>
                    <View
                      style={
                        deliveryOption === 'dinein'
                          ? [styles.radioButton, styles.activeRadioButton]
                          : [styles.radioButton, styles.inActiveRadioButton]
                      }>
                      <TouchableOpacity
                        style={
                          deliveryOption === 'dinein'
                            ? styles.circle
                            : styles.noCircle
                        }
                        onPress={() => setDeliveryOption('dinein')}
                      />
                    </View>
                  </View>
                  <View style={styles.deliveryOption}>
                    <Icon name="car" size={24} />
                    <Text style={styles.deliveryText}>Take way</Text>
                    <View
                      style={
                        deliveryOption === 'takeway'
                          ? [styles.radioButton, styles.activeRadioButton]
                          : [styles.radioButton, styles.inActiveRadioButton]
                      }>
                      <TouchableOpacity
                        style={
                          deliveryOption === 'takeway'
                            ? styles.circle
                            : styles.noCircle
                        }
                        onPress={() => setDeliveryOption('takeway')}
                      />
                    </View>
                  </View>
                </View>
              </View>
            </ScrollView>
            <View style={styles.defaultButton}>
              <TouchableOpacity
                style={styles.addButton}
                onPress={() => Alert.alert('Order Placed')}
                disabled={conApi.total === 0}>
                <Text style={[styles.buttonText, styles.viewCartText]}>
                  PLACE ORDER
                </Text>
              </TouchableOpacity>
            </View>
          </>
        )}
      </OrderFoodContext.Consumer>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  container: {
    paddingBottom: 100,
  },
  headerBackground: {
    flex: 1,
    backgroundColor: '#0e1c2b',
    height: 300,
  },
  totalCostWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  totalCost: {
    backgroundColor: '#fcfcfc',
    flexDirection: 'column',
    paddingHorizontal: 70,
    paddingVertical: 24,
    borderRadius: 10,
  },
  totalText: {
    color: '#ebb97b',
    fontSize: 18,
    fontWeight: '300',
    textAlign: 'center',
  },
  priceText: {
    paddingTop: 8,
    color: '#555656',
    textAlign: 'center',
    fontSize: 25,
    fontWeight: '500',
  },
  cartItems: {
    padding: 16,
  },
  subheading: {
    fontSize: 20,
  },
  showMoreWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  showMoreButton: {
    paddingTop: 4,
    paddingRight: 16,
    paddingBottom: 16,
  },
  showMoreText: {
    fontWeight: '300',
    fontSize: 18,
    textDecorationLine: 'underline',
    color: '#000000',
  },
  defaultButton: {
    backgroundColor: '#0e1c2b',
    position: 'absolute',
    width: '100%',
    bottom: 0,
    zIndex: 1,
    height: 70,
    justifyContent: 'center',
    flexDirection: 'column',
  },
  addButton: {
    flexDirection: 'row',
    fontSize: 18,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    textAlign: 'center',
    color: '#fcfcfc',
    fontSize: 16,
  },
  viewCartText: {
    paddingLeft: 8,
  },
  deliveryOptionsWrapper: {
    flexDirection: 'row',
    flex: 1,
    paddingHorizontal: 24,
  },
  deliveryOption: {
    flex: 5,
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingTop: 32,
  },
  radioButton: {
    width: 25,
    height: 25,
    borderRadius: 12.5,
    borderWidth: 2.5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  activeRadioButton: {
    borderColor: '#ebb97b',
  },
  inActiveRadioButton: {
    borderColor: '#808080',
  },
  circle: {
    width: 15,
    height: 15,
    backgroundColor: '#ebb97b',
    borderRadius: 13,
  },
  noCircle: {
    backgroundColor: '#fcfcfc',
    width: 15,
    height: 15,
    borderRadius: 13,
  },
  deliverySubheading: {
    paddingLeft: 16,
    fontWeight: '300',
  },
  deliveryText: {
    fontSize: 18,
    fontWeight: '300',
  },
  header: {
    flexDirection: 'row',
    paddingTop: 16,
    paddingHorizontal: 16,
  },
  heading: {
    fontSize: 24,
    color: '#fcfcfc',
    paddingLeft: 24,
  },
});

export default Cart;
