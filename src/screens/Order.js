// @flow

import React from 'react';
import {
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Dimensions,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import OrderFoodContext from '../context/orderFoodContext';
import FoodItemList from '../components/FoodItemList';
import CustomStatusBar from '../components/CustomStatusBar';
import bg from '../../assets/salad.jpeg';

const DEVICE_WIDTH = Dimensions.get('window').width;

const Order = ({navigation}) => {
  const styles = getStyles;
  const iconColor = '#555656';
  return (
    <View style={styles.wrapper}>
      <CustomStatusBar backgroundColor="#151515" barStyle="light-content" />
      <OrderFoodContext.Consumer>
        {(conApi) => (
          <View style={[styles.wrapper, styles.container]}>
            <ScrollView style={styles.wrapper}>
              <View style={styles.imageWrapper}>
                <ImageBackground source={bg} style={styles.image} />
                <View style={styles.header}>
                  <Icon
                    name="arrow-up-circle-outline"
                    color="#fcfcfc"
                    size={30}
                    style={styles.headerIconSpace}
                  />
                  <Icon
                    name="information-outline"
                    color="#fcfcfc"
                    size={30}
                    style={styles.headerIconSpace}
                  />
                </View>
              </View>
              <View style={styles.infoWrapper}>
                <Text style={[styles.restaurantName, styles.primaryColor]}>
                  Inka Restaurant
                </Text>
                <Text
                  style={[styles.restaurantTimeAndPhone, styles.secondaryText]}>
                  <Icon
                    name="star-outline"
                    size={20}
                    color={iconColor}
                    style={styles.icon}
                  />
                  5.0(200+) | All days: 09:00 AM - 06:00 PM
                </Text>
                <View style={styles.infoIconWrapper}>
                  <Icon
                    name="phone-outline"
                    size={20}
                    color={iconColor}
                    style={[styles.icon, styles.iconSpace]}
                  />
                  <Text
                    style={[
                      styles.restaurantTimeAndPhone,
                      styles.secondaryText,
                    ]}>
                    Reach us at: 9854562142
                  </Text>
                </View>
                <TouchableOpacity style={styles.tableBookButton}>
                  <Text style={styles.buttonText}>BOOK A TABLE</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.foodSectionWrapper}>
                <Text style={styles.subheading}>Starter</Text>
                {conApi.items.map((item) => (
                  <FoodItemList key={item.id} item={item} isCart={false} />
                ))}
              </View>
            </ScrollView>
            <View style={styles.menuWrapper}>
              <View style={styles.menu}>
                <Icon name="circle-slice-8" size={30} color="#0e1c2b" />
                <Text style={styles.menuText}>MENU</Text>
              </View>
            </View>
            <View style={styles.defaultButton}>
              <TouchableOpacity
                style={styles.addButton}
                onPress={() => navigation.navigate('Cart')}
                disabled={conApi.total === 0}>
                <Icon name="cart-outline" size={24} color="#ffffff" />
                <Text style={[styles.buttonText, styles.viewCartText]}>
                  VIEW CART (
                  {conApi.total > 1
                    ? conApi.total + ' ITEMS'
                    : conApi.total + ' ITEM'}
                  )
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </OrderFoodContext.Consumer>
    </View>
  );
};

const getStyles = StyleSheet.create({
  wrapper: {
    flex: 1,
    height: '100%',
    backgroundColor: '#fcfcfc',
  },
  container: {
    paddingBottom: 70,
  },
  imageWrapper: {
    height: 300,
    width: '100%',
  },
  image: {
    height: '100%',
    width: '100%',
    position: 'relative',
  },
  primaryColor: {
    color: '#0e1c2b',
  },
  infoWrapper: {
    alignItems: 'center',
    backgroundColor: '#ffffff',
    padding: 16,
    marginHorizontal: 16,
    marginTop: -32,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 5,
  },
  restaurantName: {
    fontSize: 24,
    marginBottom: 16,
  },
  restaurantTimeAndPhone: {
    fontSize: 16,
    marginBottom: 8,
  },
  tableBookButton: {
    fontSize: 18,
    width: DEVICE_WIDTH / 3,
    backgroundColor: '#0e1c2b',
    height: 40,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 8,
  },
  buttonText: {
    textAlign: 'center',
    color: '#fcfcfc',
    fontSize: 16,
  },
  secondaryText: {
    color: '#555656',
  },
  foodSectionWrapper: {
    padding: 16,
  },
  subheading: {
    fontSize: 20,
  },
  defaultButton: {
    backgroundColor: '#0e1c2b',
    position: 'absolute',
    width: '100%',
    bottom: 0,
    zIndex: 1,
    height: 70,
    justifyContent: 'center',
    flexDirection: 'column',
  },
  addButton: {
    flexDirection: 'row',
    fontSize: 18,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textXL: {
    fontSize: 18,
  },
  icon: {
    fontWeight: '300',
  },
  menuWrapper: {
    position: 'absolute',
    width: '100%',
    bottom: 0,
    zIndex: 1,
    marginBottom: 95,
    alignItems: 'center',
  },
  menu: {
    flex: 2.5,
    flexDirection: 'row',
    backgroundColor: '#ebb97b',
    paddingHorizontal: 8,
    paddingTop: 4,
    paddingBottom: 2,
    borderRadius: 8,
    color: '#0e1c2b',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,

    elevation: 7,
  },
  viewCartText: {
    paddingLeft: 8,
  },
  menuText: {
    fontSize: 18,
    paddingTop: 4,
    paddingLeft: 8,
  },
  infoIconWrapper: {
    flexDirection: 'row',
  },
  iconSpace: {
    paddingRight: 8,
  },
  header: {
    fontWeight: 'bold',
    color: 'white',
    position: 'absolute',
    top: 0,
    right: 16,
    paddingTop: 8,
    flexDirection: 'row',
  },
  headerIconSpace: {
    paddingLeft: 16,
  },
});
export default Order;
